﻿--1st, Create DB ProfileFuels

USE [ProfileFuels]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[ActivityLog](
	[ActivityTime] [datetime] NOT NULL,
	[Application] [varchar](20) NULL,
	[Methodology] [varchar](20) NULL,
	[RefineryID] [varchar](6) NOT NULL,
	[CallerIP] [varchar](20) NOT NULL,
	[UserID] [varchar](20) NULL,
	[ComputerName] [varchar](50) NULL,
	[Service] [varchar](50) NULL,
	[Method] [varchar](50) NULL,
	[EntityName] [varchar](50) NULL,
	[PeriodStart] [varchar](20) NULL,
	[PeriodEnd] [varchar](20) NULL,
	[Notes] [varchar](max) NULL,
	[Status] [varchar](50) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO

SET ANSI_PADDING ON
GO


CREATE PROC [dbo].[spLogActivity]
@Application varchar(20),
@Methodology varchar(20),
@RefineryID varchar(6), 
@CallerIP varchar(20), 
@UserID varchar(20), 
@ComputerName varchar(50), 
@Service varchar(50),
@Method varchar(50), 
@EntityName varchar(50), 
@PeriodStart Varchar(20),
@PeriodEnd varchar(20),
@Notes varchar(max),
@Status varchar(50) 
AS
INSERT INTO dbo.ActivityLog(ActivityTime, [Application], Methodology, RefineryID, CallerIP, UserID,ComputerName,  [Service], Method,EntityName,PeriodStart,PeriodEnd,Notes, Status)
VALUES(GETDATE(),@Application, @Methodology, @RefineryID, @CallerIP, @UserID, @ComputerName,  @Service, @Method, @EntityName, @PeriodStart, @PeriodEnd, @Notes, @Status)

GO

--Next, need to add user ProfileFuels to db.
GRANT SELECT, INSERT, UPDATE, DELETE ON dbo.ActivityLog to ProfileFuels
GO

GRANT EXECUTE ON dbo.spLogActivity to ProfileFuels
GO
