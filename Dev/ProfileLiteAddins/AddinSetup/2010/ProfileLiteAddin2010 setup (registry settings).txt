ProfileLiteAddin2010 setup (registry settings):
Per-User installs:


Office 32 bit
Windows 32 bit
------------------
HKEY_LOCAL_MACHINE\SOFTWARE(32-Bit)\Microsoft\Office\Excel\Addins\SolomonAssociates.ProfileLiteAddin2010

Description
FriendlyName
LoadBehavior
Manifest

ProfileLiteAddin2010
file:///[INSTALLDIR]SolomonAssociates.ProfileLiteAddin2010.vsto|vstolocal
===========================
Office 32 bit
Windows 64 bit
------------------
HKEY_LOCAL_MACHINE\SOFTWARE(32-Bit)\Microsoft\Office\Excel\Addins\SolomonAssociates.ProfileLiteAddin2010
Description
FriendlyName
LoadBehavior
Manifest

ProfileLiteAddin2010
file:///[INSTALLDIR]SolomonAssociates.ProfileLiteAddin2010.vsto|vstolocal

HKEY_LOCAL_MACHINE\SOFTWARE(64-Bit)\Microsoft\Office\Excel\Addins\SolomonAssociates.ProfileLiteAddin2010
Description
FriendlyName
LoadBehavior
Manifest

ProfileLiteAddin2010
file:///[INSTALLDIR]SolomonAssociates.ProfileLiteAddin2010.vsto|vstolocal

===========================
C:/Program Files (x86)/Solomon Associates


For Office64/win64, 
HKEY_LOCAL_MACHINE\SOFTWARE(64-Bit)\Microsoft\Office\Excel\Addins\SolomonAssociates.ProfileLiteAddin2010
Description
FriendlyName
LoadBehavior
Manifest

ProfileLiteAddin2010
file:///[INSTALLDIR]SolomonAssociates.ProfileLiteAddin2010.vsto|vstolocal
