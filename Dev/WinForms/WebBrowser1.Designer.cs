﻿namespace ProfileLiteWinForms
{
    partial class WebBrowser1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.WebBrowser = new System.Windows.Forms.WebBrowser();
            this.panel1 = new System.Windows.Forms.Panel();
            this.Reports = new System.Windows.Forms.ComboBox();
            this.Months = new System.Windows.Forms.ComboBox();
            this.PrintReport = new System.Windows.Forms.Button();
            this.ViewReport = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.btnURL = new System.Windows.Forms.Button();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // WebBrowser
            // 
            this.WebBrowser.Location = new System.Drawing.Point(0, 50);
            this.WebBrowser.MinimumSize = new System.Drawing.Size(20, 20);
            this.WebBrowser.Name = "WebBrowser";
            this.WebBrowser.Size = new System.Drawing.Size(627, 393);
            this.WebBrowser.TabIndex = 0;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.btnURL);
            this.panel1.Controls.Add(this.Reports);
            this.panel1.Controls.Add(this.Months);
            this.panel1.Controls.Add(this.PrintReport);
            this.panel1.Controls.Add(this.ViewReport);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(593, 48);
            this.panel1.TabIndex = 1;
            // 
            // Reports
            // 
            this.Reports.FormattingEnabled = true;
            this.Reports.Location = new System.Drawing.Point(177, 18);
            this.Reports.Name = "Reports";
            this.Reports.Size = new System.Drawing.Size(121, 21);
            this.Reports.TabIndex = 5;
            this.Reports.SelectedIndexChanged += new System.EventHandler(this.Reports_SelectedIndexChanged);
            // 
            // Months
            // 
            this.Months.FormattingEnabled = true;
            this.Months.Location = new System.Drawing.Point(7, 19);
            this.Months.Name = "Months";
            this.Months.Size = new System.Drawing.Size(121, 21);
            this.Months.TabIndex = 4;
            this.Months.SelectedIndexChanged += new System.EventHandler(this.Months_SelectedIndexChanged);
            // 
            // PrintReport
            // 
            this.PrintReport.Location = new System.Drawing.Point(420, 0);
            this.PrintReport.Name = "PrintReport";
            this.PrintReport.Size = new System.Drawing.Size(75, 23);
            this.PrintReport.TabIndex = 3;
            this.PrintReport.Text = "Print Report";
            this.PrintReport.UseVisualStyleBackColor = true;
            this.PrintReport.Click += new System.EventHandler(this.PrintReport_Click);
            // 
            // ViewReport
            // 
            this.ViewReport.Location = new System.Drawing.Point(328, 0);
            this.ViewReport.Name = "ViewReport";
            this.ViewReport.Size = new System.Drawing.Size(75, 23);
            this.ViewReport.TabIndex = 2;
            this.ViewReport.Text = "View Report";
            this.ViewReport.UseVisualStyleBackColor = true;
            this.ViewReport.Click += new System.EventHandler(this.ViewReport_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(181, 4);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(39, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Report";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(4, 2);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(37, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Month";
            // 
            // btnURL
            // 
            this.btnURL.Location = new System.Drawing.Point(515, 16);
            this.btnURL.Name = "btnURL";
            this.btnURL.Size = new System.Drawing.Size(57, 23);
            this.btnURL.TabIndex = 6;
            this.btnURL.Text = "URL";
            this.btnURL.UseVisualStyleBackColor = true;
            this.btnURL.Visible = false;
            this.btnURL.Click += new System.EventHandler(this.btnURL_Click);
            // 
            // WebBrowser1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(628, 447);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.WebBrowser);
            this.Name = "WebBrowser1";
            this.Text = "WebBrowser1";
            this.Resize += new System.EventHandler(this.WebBrowser1_Resize);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.WebBrowser WebBrowser;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.ComboBox Reports;
        private System.Windows.Forms.ComboBox Months;
        private System.Windows.Forms.Button PrintReport;
        private System.Windows.Forms.Button ViewReport;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnURL;
    }
}