﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Configuration;

namespace ProfileLiteWinForms
{
    public partial class WebBrowser1 : Form
    {
        //move these to config later.
        private string _reportsUrl ="https://webservices.solomononline.com/RefineryReports/ReportControl.aspx";
        private string _dataServicesUrl =   "https://webservices.solomononline.com/refineryws-2012/Data/DataServices.asmx";
        private string _clientKey = string.Empty;
        private String _refineryID = string.Empty;
        private String _location = string.Empty;
        //private String _reportCode = string.Empty;
        //private DateTime _periodStart = DateTime.MinValue;
        private String _uOM = string.Empty;
        private String _currencyCode = string.Empty;
        private String _studyYear = string.Empty;
        private String _dataSet = string.Empty;
        private String _scenario = string.Empty;
        private Boolean _includeTarget = false;
        private Boolean _includeAvg = false;
        private Boolean _includeYTD = false;
        private string _profileUrl = string.Empty;

        //how to get this back to .vb? and then to vba???
        public string WebBrowserErrors { get; set; }
        private string _windowTitle = string.Empty;


        public WebBrowser1(string windowTitle)
        {
            
            InitializeComponent();
            _windowTitle = windowTitle;
            this.Text = _windowTitle;
            try
            {
                if (ConfigurationManager.AppSettings["ShowUrl"].ToString().ToUpper() == "TRUE")
                {
                    btnURL.Visible = true;
                }
            }
            catch { }

        }

        public string PrepForm(string clientKey, String refineryID, String location,
            String UOM, String currencyCode, String studyYear,
            String dataSet, String Scenario, Boolean includeTarget, Boolean includeAvg, Boolean includeYTD)
        {
            WebBrowserErrors = string.Empty;
            string errors = string.Empty;
            try
            {
                _clientKey = clientKey;
                _refineryID = refineryID;
                _location = location;
                //_reportCode = reportCode;
                //_periodStart = periodStart;
                _uOM = UOM;
                _currencyCode = currencyCode;
                _studyYear = studyYear;
                _dataSet = dataSet;
                _scenario = Scenario;
                _includeTarget = includeTarget;
                _includeAvg = includeAvg;
                _includeYTD = includeYTD;

                ViewReport.Visible = false;
                PrintReport.Visible = false;

                PopulateReportsCbo();
                errors = PopulateMonthsCbo(clientKey);
                if (errors.Length<1)
                {
                    ViewReport.Visible = true;
                    PrintReport.Visible = true;
                }
                //this.Visible = true;
                //WebBrowser1 temp = new WebBrowser1();
                //temp.ShowDialog(this);

                //this.ShowDialog();

            }
            catch (Exception ex)
            {
                errors = "Error in PrepForm(): " + ex.Message;
            }
            WebBrowser.Width = this.Width - 20;
            WebBrowser.Height = this.Height - 100;
            WebBrowserErrors = errors;
            return errors;
        }

        private string PopulateMonthsCbo(string clientKey)
        {
            string errors = string.Empty;
            DataSet ds = null;
            try
            {
                //* <add key="DataServicesUrl" value="https://webservices.solomononline.com/refineryws-2012/Data/DataServices.asmx" />
                _dataServicesUrl = ConfigurationManager.AppSettings["DataServicesUrl"].ToString();
            }
            catch{}

            //BUT PROBABLY NEED CERTIFICATE !!!!
            DataServices.DataServices dataServices= new DataServices.DataServices();
            dataServices.Url = _dataServicesUrl;
            try
            {
                ds = dataServices.GetReferences(_refineryID);
            }
            catch (Exception ex)
            {
                errors = "Error in POpulateMonthsCbo(): " + ex.Message;
            }
            if (errors.Length > 0)
                return errors;
            if (ds != null && ds.Tables.Count > 0 && ds.Tables.Contains("LoadedMonths") && ds.Tables["LoadedMonths"].Rows.Count > 0)
            {
                foreach (DataRow row in ds.Tables["LoadedMonths"].Rows)
                {
                    Months.Items.Add((new DateTime(2015, Int32.Parse(row[1].ToString()), 1)).ToString("MMMM") + " " + row[0].ToString());
                }
            }
            WebBrowserErrors += errors;
            return errors;
        }

        private void PopulateReportsCbo()
        {
            string reportsList = "Trend Report^Process Unit Scorecard^EDC, Utilization, Standard Energy and Gain^EII and VEI Details^Turnarounds^Turnaround Annualization^Monthly Operational Availability^Study Equivalent Operational Availability" ;
            try
            {
                reportsList = ConfigurationManager.AppSettings["AvailableReports"].ToString();
            }
            catch{}
            string[] parts = reportsList.Split('^');
            for (int i = 0; i < parts.Length; i++)
            {
                Reports.Items.Add(parts[i].ToString());
            }
        }

        private void ViewReport_Click(object sender, EventArgs e)
        {
            //this shouldnt happen
            //if(Months.Text.Length<1)
            //if(Reports.Text.Length<1)
           // ClearBrowser();
            WebBrowserErrors = string.Empty;
            string selectedMonth = Months.Text;
            string[] parts = Months.Text.Split(' ');
            string discardDate = parts[0] + " 1, 2015";
            int month = Convert.ToDateTime(discardDate).Month; //"8/1/2015", "MMM", System.Globalization.CultureInfo.InvariantCulture);
            DateTime dateToUse= new DateTime(Int32.Parse(parts[1]), month, 1);
            string errors = DisplayReport(dateToUse, Reports.Text);
            if (errors.Length > 0)
            {
                WebBrowserErrors += errors;
            }
        }

        private string DisplayReport(DateTime periodStart, String reportCode)
        {
            string errors = string.Empty;
            //WebBrowserErrors = string.Empty;
            try
            {
                try
                {
                    //* <add key="DataServicesUrl" value="https://webservices.solomononline.com/RefineryReports/ReportControl.aspx" />
                    _reportsUrl = ConfigurationManager.AppSettings["ReportsUrl"].ToString();
                }
                catch { }

                string target= _includeTarget? "True" : "False";
                string average= _includeAvg? "True" : "False";
                string ytd= _includeYTD? "True" : "False";

                //string CleanKey = Replace(clientKey, "/", "%2F", 1)
                //CleanKey = Replace(CleanKey, "+", "%2B", 1, , vbBinaryCompare)
                string CleanKey = _clientKey.Replace( "/", "%2F");
                CleanKey = CleanKey.Replace("+", "%2B");

                string url = _reportsUrl + "?" +
                "rn=" + reportCode +
                "&sd=" +       periodStart.ToString("M/d/yyyy") +  //     String.Format("M/d/yyyy", periodStart.ToString()) +
                "&ds=" + _dataSet +
                "&UOM=" + _uOM +
                "&currency=" + _currencyCode +
                "&yr=" + _studyYear +
                "&target=" + target +
                "&avg=" + average +
                "&ytd=" + ytd +
                "&SN=" + _scenario +
                "&TS=" + DateTime.Now.ToString("M/d/yyyy  hh:mm:ss tt") + //String.Format("m/d/yyyy hh:mm:ss AMPM", DateTime.Now.ToString()) +   // Format(dt, "m/d/yyyy hh:mm:ss AMPM") +
                "&WsP=" + CleanKey;
                _profileUrl = url;
                //appears to be appended to conx string . . . string header = "WsP: " + CleanKey + Environment.NewLine;
                WebBrowser.Navigate(url); //,null,null, header);
            }
            catch(Exception ex)
            {
                errors = "Error in DisplayReport(): " + ex.Message;
            }
            //WebBrowserErrors = errors;
            return errors;
        }

        private void PrintReport_Click(object sender, EventArgs e)
        {
            //don't seem to be able to easily remove URL from page:
            //http://stackoverflow.com/questions/31052656/remove-header-and-footer-when-printing-from-webbrowser-control
            //http://stackoverflow.com/questions/15611479/webbrowser-class-how-to-print-document-without-header-footer
            WebBrowser.ShowPrintPreviewDialog();
        }

        private void Months_SelectedIndexChanged(object sender, EventArgs e)
        {
            ClearBrowser();
        }

        private void ClearBrowser()
        {
            WebBrowser.Navigate("about:blank");
            WebBrowser.Document.OpenNew(true);
            WebBrowser.Document.Write("");
            //WebBrowser.Url = null;
            //WebBrowser.Refresh();
        }

        private void Reports_SelectedIndexChanged(object sender, EventArgs e)
        {
            //while (WebBrowser.DocumentText.Length > 10)
            //{
            //    ClearBrowser();
            //    System.Threading.Thread.Sleep(500);
            //    WebBrowser.DocumentText = "";
            //}

            ClearBrowser();
           // ClearBrowser();
        }

        private void WebBrowser1_Resize(object sender, EventArgs e)
        {
            WebBrowser.Width = this.Width - 20;
            WebBrowser.Height = this.Height- 100;
        }

        private void btnURL_Click(object sender, EventArgs e)
        {
            MessageBox.Show(_profileUrl);
        }


        /*
        <add key="AvailableReports" value="Trend Report^Process Unit Scorecard^EDC, Utilization, Standard Energy and Gain^EII and VEI Details^Turnarounds^Turnaround Annualization^Monthly Operational Availability^Study Equivalent Operational Availability" />
        <add key="Trend Report" value="Trend Report" />
        <add key="Process Unit Scorecard" value="Delek Process Unit Scorecard" />
        <add key="EDC, Utilization, Standard Energy and Gain" value="EDC, Utilization, Standard Energy and Gain" />
        <add key="EII and VEI Details" value="EII and VEI Details" />
        <add key="Turnarounds" value="Turnarounds" />
        <add key="Turnaround Annualization" value="Turnaround Annualization" />
        <add key="Monthly Operational Availability" value="Monthly Operational Availability" />
        <add key="Study Equivalent Operational Availability" value="Study Equivalent Operational Availability" />

        */

    }
}
