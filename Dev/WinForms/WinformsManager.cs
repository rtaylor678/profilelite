﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Reflection;
using System.Configuration;

namespace ProfileLiteWinForms
{
    public class WinformsManager
    {
        string _windowTitle = string.Empty;


        public WinformsManager(string title)
        {
            _windowTitle = title;
        }

        WebBrowser1 form = null;

        public string ShowReport(string clientKey, String refineryID, String location,
            String UOM, String currencyCode, String studyYear,
            String dataSet, String Scenario, Boolean includeTarget, Boolean includeAvg, Boolean includeYTD)
        {
            string errors = string.Empty;
            string progress = "1";
            try
            {
                progress += "2";
                form = new WebBrowser1(_windowTitle);
                progress += "3";
                try
                {
                    progress += "4";
                    Assembly assembly = Assembly.GetCallingAssembly();
                }
                catch { }
                progress += "5";
                try
                { //"ONLY USE THIS BLOCK FOR UNIT TESTING")
                    progress += "6";
                    if (ConfigurationManager.AppSettings["UnitTesting"].ToString().ToUpper() == "TRUE")
                    {
                        form.Load += (sender, e) => (sender as WebBrowser1).Visible = true;
                    }
                }
                catch { }
                progress += "7";
                errors = form.PrepForm(clientKey, refineryID, location, UOM, currencyCode, studyYear, dataSet, Scenario, includeTarget, includeAvg, includeYTD);
                progress += "8";
                if (errors.Length > 0)
                    return form.WebBrowserErrors;
                progress += "9";
                form.ShowDialog();
                progress += "10";
                return form.WebBrowserErrors;

            }
            catch (Exception ex)
            {
                errors = "Error in ShowReport(): " + ex.Message + " " + progress;
            }
            return errors;
        }

        private string ShowReportUsingUnitTest(string clientKey, String refineryID, String location,
            String UOM, String currencyCode, String studyYear,
            String dataSet, String Scenario, Boolean includeTarget, Boolean includeAvg, Boolean includeYTD)
        {
            string errors = string.Empty;
            try
            {
                form = new WebBrowser1(_windowTitle);
                bool calledFromUnitTest = false;
                try
                {
                    Assembly assembly = Assembly.GetCallingAssembly();
                    if (refineryID == "XXPAC") // (assembly.FullName.Contains("ProfileLiteAddinBase.Test"))
                        calledFromUnitTest = true;
                }
                catch { }

                if (calledFromUnitTest)
                {
                    //only use this line for unit testing to show form.
                    form.Load += (sender, e) => (sender as WebBrowser1).Visible = true;
                    errors = form.PrepForm(clientKey, refineryID, location, UOM, currencyCode, studyYear, dataSet, Scenario, includeTarget, includeAvg, includeYTD);
                    if (errors.Length > 0)
                        return form.WebBrowserErrors;
                    form.ShowDialog();
                    return form.WebBrowserErrors;
                }
                else
                {
                    errors = form.PrepForm(clientKey, refineryID, location, UOM, currencyCode, studyYear, dataSet, Scenario, includeTarget, includeAvg, includeYTD);
                    if (errors.Length > 0)
                        return form.WebBrowserErrors;
                    form.ShowDialog();
                    return form.WebBrowserErrors;
                }
            }
            catch (Exception ex)
            {
                errors = "Error in ShowReport(): " + ex.Message;
            }
            return errors;
        }        

    }
}

