﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
//using ClientKeyGeneratorApplication.WsXmlProLite;
using System.Reflection;
using System.Text.RegularExpressions;


namespace ClientKeyGeneratorApplication
{
    public partial class ClientGenForm : Form
    {
        private string encryptedContent { get; set; }// = string.Empty;

        public ClientGenForm()
        {
            InitializeComponent();
            txtPassword.GotFocus += txtPassword_GotFocus;
            txtPassword.GotFocus += txtPassword_LostFocus;
        }

        private void btnGenerate_Click(object sender, EventArgs e)
        {
            if (ValidateContent() == false) return;
            string keyText = BuildClientKey();
            
            //Now call service to encrypt
            string encryptedText = EncryptClientKey(keyText);

            if (encryptedText.Length > 0)
            {
                if (Directory.Exists(@"C:\ClientKeys\") != true) { Directory.CreateDirectory(@"C:\ClientKeys\"); }
                saveFileDialog1.CheckPathExists = true;
                saveFileDialog1.InitialDirectory = @"C:\ClientKeys\";
                saveFileDialog1.DefaultExt = "key";
                saveFileDialog1.FileName = txtCompany.Text.Trim() + " (" + this.txtRefNum.Text.Trim() + ")" + ".key";    

                if (saveFileDialog1.ShowDialog() == DialogResult.OK) 
                { 
                   string fileSave= saveFileDialog1.FileName;
                   SaveFileContent(fileSave, encryptedText);
                   string message = "File successfully saved to : " + fileSave;
                   MessageBox.Show(message);

                   txtRefNum.Text = txtRefNum.Text.ToUpper();

                   lblResult.Text = message;

                   lblResult.Visible = true;
                   lblResultLabel.Visible = true;
                   lblGenKey.Visible = true;
                   txtGenKey.Visible = true;
                   txtGenKey.Text = encryptedText;

                   encryptedContent = encryptedText;

                   //DWW  12-29-2015 TODO:
                   CreatePfx();
                   CreateCert();

                } 
            }
        }


        private void CreatePfx()
        {

        }

        private void CreateCert()
        {

        }


        private void ClearText()
        {
            txtCompany.Text = string.Empty;
            txtPassword.Text = string.Empty;
            txtLocation.Text = string.Empty;
            txtRefNum.Text = string.Empty;
            lblResult.Visible = false;
            lblResultLabel.Visible = false;
            lblGenKey.Visible = false;
            txtGenKey.Visible = false;
            txtGenKey.Text = string.Empty;
        }

        private bool ValidateContent()
        {
            
            if (txtCompany.Text == string.Empty)
            {
                MessageBox.Show("Company Name is required");
                txtCompany.Focus();
                txtCompany.Select();
                return false;
            }

            if (txtPassword.Text == string.Empty)
            {
                MessageBox.Show("Password is required");
                txtCompany.Focus();
                txtCompany.Select();
                return false;
            }

            if (txtLocation.Text == string.Empty)
            {
                MessageBox.Show("Location is required");
                txtLocation.Focus();
                txtLocation.Select();
                return false;
            }


            if (txtRefNum.Text == string.Empty)
            {
                MessageBox.Show("Ref Num is required");
                txtRefNum.Focus();
                txtLocation.Select();
                return false;
            }

            if (IsValidPassword() != true)
            {
                MessageBox.Show("Password is invalid");
                lblPwdMsg.Visible = true;
                txtPassword.Focus();
                txtPassword.Select();
                return false;

            }

            return true;
        }

        private string BuildClientKey()
        {
            string retVal = string.Empty;

            if (txtPassword.Text.Length > 0)
            {
                retVal = txtCompany.Text.Trim() + "$" + txtPassword.Text.Trim() + "$" + txtLocation.Text.Trim() + "$" + txtRefNum.Text.ToUpper().Trim();
            }
            else
            {
                retVal = txtCompany.Text.Trim() + "$" + txtLocation.Text.Trim() + "$" + txtRefNum.Text.ToUpper().Trim();
            }

            return retVal;
        }

        private string EncryptClientKey(string keyText)
        {
            return  EncryptClientKeyValue(keyText);
        }

        public string EncryptClientKeyValue(string encryptText )
        {
            ProfileLiteSecurity.Cryptography.Cryptographer encrypt = new ProfileLiteSecurity.Cryptography.Cryptographer();
            return encrypt.Encrypt(encryptText);
        }

        
       private void SaveFileContent(string fileSave, string encryptedText)
        {

            FileStream fs1 = new FileStream(fileSave, FileMode.OpenOrCreate, FileAccess.Write);
            StreamWriter writer = new StreamWriter(fs1);
            writer.Write(encryptedText);
            writer.Close();


        }

        private void checkWebServiceToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //string retVal = string.Empty;

            //using (WsXmlProLite.ProfileLiteWebServices client = new WsXmlProLite.ProfileLiteWebServices())
            //{

            //    retVal = client.CheckService();

            //    MessageBox.Show(retVal);
            //}
        }


        private void txtPassword_GotFocus(object sender, EventArgs e)
        {
            lblPwdMsg.Visible = true;
        }

        private void txtPassword_LostFocus(object sender, EventArgs e)
        {
            lblPwdMsg.Visible = false;
        }

        private bool IsValidPassword()
        {
            bool isValid = true;

            if (txtPassword.Text.Trim().Length < 8) { isValid = false; return isValid; }
            if (txtPassword.Text.Trim().Length > 32) { isValid = false; return isValid; }
            
            string pattern = @"[a-zA-Z0-9]";
            int matches = Regex.Matches(txtPassword.Text, pattern).Count;
            int length = txtPassword.Text.Trim().Length;

            if (matches != length) { isValid = false; return isValid; }
             
            return isValid;


        }

        private void txtRefNum_TextChanged(object sender, EventArgs e)
        {

        }

        private void btnClear_Click(object sender, EventArgs e)
        {
            ClearText();
        }

        private void certificateToolStripMenuItem_Click(object sender, EventArgs e)
        {
            
            if (encryptedContent == string.Empty)
            {
                Form form = new TestCertificateForm();
                form.Show();
            }
            else
            {
                Form form = new TestCertificateForm(encryptedContent);
                form.Show();
            }
            
          
        }
       
    }
}
