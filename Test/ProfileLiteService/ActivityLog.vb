﻿Imports System.String
Imports System.IO
Imports System.Collections.Generic
Imports System.Data.SqlClient

Public Class ActivityLog


    Public Sub Log(Methodology As String, refineryID As String, CallerIP As String, UserID As String, ComputerName As String, Service As String, Method As String, EntityName As String, PeriodStart As String, PeriodEnd As String, Notes As String, status As String)
        DBHelper.RefineryID = refineryID
        DBHelper.QueryDb("spLogActivity 'Profile Lite','" & DBHelper.CleanText(Methodology) & "','" & DBHelper.CleanText(refineryID) & "','" & DBHelper.CleanText(CallerIP) & "','" & DBHelper.CleanText(UserID) & _
                   "','" & DBHelper.CleanText(ComputerName) & _
                   "', '" & DBHelper.CleanText(Service) & "','" & DBHelper.CleanText(Method) & "','" & DBHelper.CleanText(EntityName) & "','" & DBHelper.CleanText(PeriodStart) & _
                   "','" & DBHelper.CleanText(PeriodEnd) & "','" & DBHelper.CleanText(Notes) & "','" & DBHelper.CleanText(status) & "'")

    End Sub

    Public Function ReadLog(refineryID As String, CallerIP As String, Optional ByVal Methodology As String = "", Optional ByVal UserID As String = "", _
                            Optional ByVal ComputerName As String = "", Optional ByVal Service As String = "", _
                            Optional ByVal Method As String = "", Optional ByVal EntityName As String = "", _
                            Optional ByVal PeriodStart As String = "", Optional ByVal PeriodEnd As String = "", _
                            Optional ByVal Notes As String = "", Optional ByVal status As String = "") As String
        'Used for diagnostics only
        DBHelper.RefineryID = refineryID
        DBHelper.QueryDb("spLogActivity 'Profile Lite','" & DBHelper.CleanText(Methodology) & "','" & DBHelper.CleanText(refineryID) & "','" & DBHelper.CleanText(CallerIP) & "','" & DBHelper.CleanText(UserID) & _
                   "','" & DBHelper.CleanText(ComputerName) & _
                   "', '" & DBHelper.CleanText(Service) & "','" & DBHelper.CleanText(Method) & "','" & DBHelper.CleanText(EntityName) & "','" & DBHelper.CleanText(PeriodStart) & _
                   "','" & DBHelper.CleanText(PeriodEnd) & "','" & DBHelper.CleanText(Notes) & "','" & DBHelper.CleanText(status) & "'")
        Dim activityTime As DataSet = DBHelper.QueryDb("select TOP 1 ActivityTime from dbo.ActivityLog where RefineryID = '" + refineryID + "' order by ActivityTime desc;")
        If Not IsNothing(activityTime) AndAlso activityTime.Tables.Count = 1 AndAlso activityTime.Tables(0).Rows.Count = 1 Then
            Return activityTime.Tables(0).Rows(0)("ActivityTime").ToString()
        Else
            Return "No record found"
        End If
    End Function

End Class

