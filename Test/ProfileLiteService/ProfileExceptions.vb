Namespace ProfileExceptions
    Public Class CalcsAlreadyRunningException
        Inherits System.ApplicationException

        Public Sub New(ByVal message As String)
            MyBase.New(message)
        End Sub
    End Class

    Public Class NoSubmissionIDException
        Inherits System.ApplicationException
        Public Sub New(ByVal message As String)
            MyBase.New(message)
        End Sub
    End Class
End Namespace

