﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ProfileLiteSecurity;
using System.Security.Cryptography.X509Certificates;

namespace ProfileLiteSecurity.Tests
{
    [TestClass]
    public class ProfileLiteSecurityTests
    {
        [TestMethod]
        public void GetClientKeyInfoFromTokenTest()
        {
            try
            {
                string clientKey = "V9UQixOjBFPRDQk6VYM0NnAryy+FvA/so2iqsYwtJZPfqQfexPjFcYaJS9Z7XlO5";
                X509Certificate2 publicCert = new X509Certificate2();
                ProfileLiteSecurity.Cryptography.Certificate pcc = new Cryptography.Certificate();
                ProfileLiteSecurity.Client.ClientKey proLiteSecurityClientKey =
                     new ProfileLiteSecurity.Client.ClientKey();
                proLiteSecurityClientKey = proLiteSecurityClientKey.GetClientKeyInfoFromToken(clientKey);
                string name = proLiteSecurityClientKey.Company;
                string refineryId = proLiteSecurityClientKey.RefNum;
                Assert.IsTrue(name == "YASREF" && refineryId == "355EUR");
            }
            catch (Exception ex)
            {
                string msg = ex.Message;
                Assert.Fail();
            }
        }

        [TestMethod]
        public void GetCertificateTest()
        {
            byte[] encodedSignedCms = new byte[0];
            string clientKey = "V9UQixOjBFPRDQk6VYM0NnAryy+FvA/so2iqsYwtJZPfqQfexPjFcYaJS9Z7XlO5";
            string name = "YASREF (355EUR)";
            X509Certificate2 privateX509Cert = new X509Certificate2();
            ProfileLiteSecurity.Cryptography.Certificate certHelper = new ProfileLiteSecurity.Cryptography.Certificate();
            bool success = certHelper.Get(name, out privateX509Cert, false, true);
            if (success != true)
                Assert.Fail();

            ProfileLiteSecurity.Cryptography.NonRepudiation nonRep = new ProfileLiteSecurity.Cryptography.NonRepudiation();
            string errorMsg = string.Empty;
            byte[] messageBytes = System.Text.Encoding.ASCII.GetBytes(clientKey);

            Assert.IsTrue(nonRep.Sign(ref errorMsg, messageBytes, privateX509Cert, out encodedSignedCms, true));
        }

        [TestMethod]
        public void VerfyCertTest()
        {
            string clientKey = "V9UQixOjBFPRDQk6VYM0NnAryy+FvA/so2iqsYwtJZPfqQfexPjFcYaJS9Z7XlO5";
            string nameWithRefNum = "YASREF (355EUR)";


            //NOTE:  If this errors, then try these steps:
            //1. Run the project, so the Web Service's page shows.
            //2. Open another instance of IE and pull up the web services' page.
            //3. Shut down the project, but leave the other instance of IE up.
            //4. Run the unit test.
            using (ProfileLiteWebService.ProfileLiteWebServices client = new ProfileLiteWebService.ProfileLiteWebServices())
            {
                X509Certificate2 x509 = new X509Certificate2();
                string errorResponse = string.Empty;
                ProfileLiteSecurity.Cryptography.Certificate certHelper  = new Cryptography.Certificate();
                byte[] encodedSignedCms = certHelper.GetEncodedSignedCmsFromNameWithRefnum( nameWithRefNum, clientKey) ; 
                bool success = client.AuthenticateByCertificateAndClientId(ref errorResponse, encodedSignedCms, clientKey, true);
                Assert.IsTrue(success);
            }
        }

        [TestMethod]
        public void FailGetClientKeyInfoFromTokenTest()
        {
            //string clientKey = "z9UQixOjBFPRDQk6VYM0NnAryy+FvA/so2iqsYwtJZPfqQfexPjFcYaJS9Z7XlO5";
            string clientKey = "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa";
            X509Certificate2 publicCert = new X509Certificate2();
            ProfileLiteSecurity.Cryptography.Certificate pcc = new Cryptography.Certificate();
            ProfileLiteSecurity.Client.ClientKey proLiteSecurityClientKey =
                    new ProfileLiteSecurity.Client.ClientKey();
            try
            {
                proLiteSecurityClientKey = proLiteSecurityClientKey.GetClientKeyInfoFromToken(clientKey);
                Assert.Fail();
            }
            catch (Exception passTestEx)
            {
                Assert.IsTrue(passTestEx.Message.Contains("Bad Data."));
            }                            
        }

        [TestMethod]
        public void FailGetCertificateTest()
        {
            byte[] encodedSignedCms = new byte[0];
            string name = "MUNICHRE (123EUR)";
            X509Certificate2 privateX509Cert = new X509Certificate2();
            ProfileLiteSecurity.Cryptography.Certificate certHelper = new ProfileLiteSecurity.Cryptography.Certificate();
            bool success = certHelper.Get(name, out privateX509Cert, false, true);
            Assert.IsTrue(!success);
        }

        [TestMethod]
        public void FailAuthenticateByCertificateAndClientIdTest1()
        {
            string clientKey = "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa";
            
            
            //string nameWithRefNum = "YASREF (355EUR)";
            //NOTE:  If this errors, then try these steps:
            //1. Run the project, so the Web Service's page shows.
            //2. Open another instance of IE and pull up the web services' page.
            //3. Shut down the project, but leave the other instance of IE up.
            //4. Run the unit test.
            using (ProfileLiteWebService.ProfileLiteWebServices client = new ProfileLiteWebService.ProfileLiteWebServices())
            {
                X509Certificate2 x509 = new X509Certificate2();
                string errorResponse = string.Empty;                
                //ProfileLiteSecurity.Cryptography.Certificate certHelper = new Cryptography.Certificate();
                byte[] encodedSignedCms = null;//certHelper.GetEncodedSignedCmsFromNameWithRefnum(nameWithRefNum, clientKey);
                bool success = client.AuthenticateByCertificateAndClientId(ref errorResponse, encodedSignedCms, clientKey, true);
                Assert.IsTrue(!success);
            }
        }

    }
}
