﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Security.Cryptography;
using System.Security.Cryptography.X509Certificates;
using System.Security.Permissions;
using System.Threading.Tasks;


namespace ProfileLiteSecurity.Cryptography
{
    [Serializable]
	public class Certificate
	{
        
        public Certificate()
        {

        }

		public bool Get(string name, out X509Certificate2 certificate, bool trustedPeople = false, bool personal=false)
		{
            bool retVal = false;
			try
			{
                X509Store store = null;
                if (trustedPeople )
                {
                    store =new X509Store(StoreName.TrustedPeople, StoreLocation.LocalMachine);
                }
                else if (personal)
                {
                    store = new X509Store(StoreName.My, StoreLocation.LocalMachine);
                }
                else
                {
                    store = new X509Store(StoreName.TrustedPeople, StoreLocation.LocalMachine);
                }

                store.Open(OpenFlags.ReadOnly);
                X509Certificate2Collection certColl = store.Certificates.Find(X509FindType.FindBySubjectName, name, true);

                if (certColl.Count > 0)
                {
                    certificate = certColl[0];
                    store.Close();
                    certColl.Clear();
                    retVal = true;
                }
                else
                {
                    certificate = null;
                    store.Close();
                    certColl.Clear();
                }                
                return retVal;
			}
			catch(Exception ex)
			{
				certificate = new X509Certificate2();
				return false;
			}
		}

        public byte[] GetEncodedSignedCmsFromNameWithRefnum(string nameWithRefNum, string clientKey)
        {
            X509Certificate2 privateX509Cert = new X509Certificate2();
            ProfileLiteSecurity.Cryptography.Certificate certHelper = new ProfileLiteSecurity.Cryptography.Certificate();
            bool success = certHelper.Get(nameWithRefNum, out privateX509Cert, false, true);

            ProfileLiteSecurity.Cryptography.NonRepudiation nonRep = new ProfileLiteSecurity.Cryptography.NonRepudiation();
            string errorMsg = string.Empty;
            byte[] messageBytes = System.Text.Encoding.ASCII.GetBytes(clientKey);
            byte[] encodedAndSignedCms = new byte[0];
            if (!nonRep.Sign(ref errorMsg, messageBytes, privateX509Cert, out encodedAndSignedCms, true))
            {
                return null;
            }
            else
            {
                return encodedAndSignedCms;
            }
        }
	}
}