﻿Imports System.Web.Services
Imports System.Web.Services.Protocols
Imports System.ComponentModel
Imports System.Data
Imports System.Data.Common
Imports System.Data.SqlClient
Imports System.IO
Imports System.Text
Imports System.Configuration
Imports ProfileLiteWebServices.UserAuthentication
Imports ProfileLiteWebServices.ProfileLiteWebServices
Imports System.Security.Cryptography
Imports System.Security.Cryptography.Pkcs
Imports System.Security.Cryptography.X509Certificates
'Imports ProfileLiteWebServices.ProLiteSecurity
Imports ProfileLiteSecurity.Client
Imports ProfileLiteWebServices.Cryptographer



'Enum RSCRUDE
'    RAIL = 100001
'    TT = 100002
'    TB = 100003
'    OMB = 100004
'    BB = 100005
'    PL = 100006
'End Enum


'Enum RSPROD
'    RAIL = 100010
'    TT = 100011
'    TT0 = 100012
'    TB = 100013
'    OMB = 100014
'    BB = 100015
'    PL = 100016
'End Enum


' To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line.
' <System.Web.Script.Services.ScriptService()> _
<System.Web.Services.WebService(Namespace:="http://solomononline.com/ProfileLiteWS/")> _
<System.Web.Services.WebServiceBinding(ConformsTo:=WsiProfiles.BasicProfile1_1)> _
<ToolboxItem(False)> _
Public Class ProfileLiteWebServices
    Inherits System.Web.Services.WebService

    'Friend WithEvents SqlConnection1 As System.Data.SqlClient.SqlConnection
    'Friend WithEvents sdSubmissions As System.Data.SqlClient.SqlDataAdapter
    'Friend WithEvents scCompleteSubmission As System.Data.SqlClient.SqlCommand
    'Friend WithEvents sdMaterialCategory_LU As System.Data.SqlClient.SqlDataAdapter
    'Friend WithEvents scStartUpload As System.Data.SqlClient.SqlCommand
    'Friend WithEvents scClearUploading As System.Data.SqlClient.SqlCommand
    'Friend WithEvents scSubmissionID As System.Data.SqlClient.SqlCommand
    'Friend WithEvents SqlSelectCommand21 As System.Data.SqlClient.SqlCommand
    'Friend WithEvents SqlInsertCommand21 As System.Data.SqlClient.SqlCommand
    'Friend WithEvents SqlUpdateCommand20 As System.Data.SqlClient.SqlCommand
    'Friend WithEvents SqlDeleteCommand20 As System.Data.SqlClient.SqlCommand
    'Friend WithEvents SqlSelectCommand1 As System.Data.SqlClient.SqlCommand
    'Friend WithEvents SqlInsertCommand1 As System.Data.SqlClient.SqlCommand
    'Friend WithEvents SqlUpdateCommand1 As System.Data.SqlClient.SqlCommand
    'Friend WithEvents SqlDeleteCommand1 As System.Data.SqlClient.SqlCommand
    'Friend WithEvents sqlLoadSubmission As System.Data.SqlClient.SqlCommand
    'Friend WithEvents scPrepareForUpload As System.Data.SqlClient.SqlCommand
    'Friend WithEvents SqlSelectCommand18 As System.Data.SqlClient.SqlCommand
    'Friend WithEvents SqlInsertCommand18 As System.Data.SqlClient.SqlCommand
    'Friend WithEvents SqlUpdateCommand25 As System.Data.SqlClient.SqlCommand
    'Friend WithEvents SqlDeleteCommand24 As System.Data.SqlClient.SqlCommand
    'Friend WithEvents SqlUpdateCommand24 As System.Data.SqlClient.SqlCommand
    'Friend WithEvents sqlAuthentication As System.Data.SqlClient.SqlCommand

    Private classErr As String
    'Private ActivityLog As New ActivityLog()
    Private _service As ProfileLiteService.Service


    Public Sub New()
        MyBase.New()

        'This call is required by the Web Services Designer.
        InitializeComponent()
        'Add your own initialization code after the InitializeComponent() call

        _service = New ProfileLiteService.Service(Context)

    End Sub

    '<WebMethod()> _
    'Public Function RunTests() As Boolean
    'Dim tests As New Tests()
    'Return tests.UserAuthenticationTest1()
    'End Function

    <WebMethod()> _
    Public Sub TestWS()
        _service.WriteLog("TEST")
    End Sub

    <WebMethod()> _
    Public Function CheckService() As String

        Return "CheckService = Success!"

    End Function

    <WebMethod()> _
    Public Function AuthorizeRefineryLogin(ByVal clientKey As String, ByVal Password As String) As Boolean
        Return _service.AuthorizeRefineryLogin(clientKey, Password)
    End Function
    <WebMethod()> _
    Public Function ChangeRefineryPassword(ByVal clientKey As String, ByVal Password As String, ByVal NewPassword As String) As Boolean
        Return _service.ChangeRefineryPassword(clientKey, Password, NewPassword)
    End Function

    'Private Sub WriteLog(st As String)
    '    _service.WriteLog(st)
    'End Sub

    'Deprecated per Richard's suggestion.
    <WebMethod()> _
    Public Sub SendTracing(ByVal clientDomain As String, ByVal traceInfo As String)
    End Sub



    <WebMethod()> _
    Public Sub AuthorizeAndSubmitData(ByVal ds As DataSet, ByVal clientKey As String, ByVal encodedSignedCms As Byte())
        _service.AuthorizeAndSubmitData(ds, clientKey, encodedSignedCms)
    End Sub

    <WebMethod()> _
    Public Sub SubmitData(ByVal ds As DataSet, ByVal clientKey As String)
        _service.SubmitData(ds, clientKey)
    End Sub

    'Private Sub SubmitDataToDB(ByVal ds As DataSet, ByVal clientKey As String)
    '    _service.SubmitDataToDB(ds, clientKey)
    'End Sub


    'Private Function GetMonthText(mo As Integer) As String
    '    _service.GetMonth()
    '    Select Case mo
    '        Case 1 : Return "JANUARY"
    '        Case 2 : Return "FEBRUARY"
    '        Case 3 : Return "MARCH"
    '        Case 4 : Return "APRIL"
    '        Case 5 : Return "MAY"
    '        Case 6 : Return "JUNE"
    '        Case 7 : Return "JULY"
    '        Case 8 : Return "AUGUST"
    '        Case 9 : Return "SEPTEMBER"
    '        Case 10 : Return "OCTOBER"
    '        Case 11 : Return "NOVEMBER"
    '        Case 12 : Return "DECEMBER"
    '        Case Else : Return ""
    '    End Select

    'End Function

    'Private Function GetMaterialName(ByRef conn As SqlConnection, ByRef MaterialId As String) As String
    '    Dim cmd As New SqlCommand()
    '    cmd.CommandType = CommandType.Text
    '    cmd.CommandText = "select SAIName FROM Material_LU where MaterialID='" + MaterialId + "'"
    '    cmd.Connection = conn
    '    Dim result As String = ""
    '    Try
    '        Dim dr As SqlDataReader = cmd.ExecuteReader()
    '        If dr.HasRows Then
    '            result = dr("SAIName").ToString()
    '        End If
    '    Catch ex As Exception
    '        'return blank, which will signify not found or error
    '    End Try
    '    Return result
    'End Function


    <WebMethod()> _
    Public Sub WriteActivityLog(Scenario As String, RefineryID As String, CallerIP As String, UserID As String, ComputerName As String, Service As String, Method As String, EntityName As String, PeriodStart As String, PeriodEnd As String, Notes As String, Status As String)
        _service.WriteActivityLog(Scenario, RefineryID, CallerIP, UserID, ComputerName, Service, Method, EntityName, PeriodStart, PeriodEnd, Notes, Status)
        'ActivityLog.Log(Scenario, RefineryID, CallerIP, UserID, ComputerName, Service, Method, EntityName, PeriodStart, PeriodEnd, Notes, Status)
    End Sub

    <WebMethod()> _
    Public Function ReadActivityLog(RefineryID As String, CallerIP As String, Scenario As String, _
                                    UserID As String, ComputerName As String, _
                                    Service As String, Method As String, _
                                    EntityName As String, PeriodStart As String, _
                                    PeriodEnd As String, Notes As String, _
                                    Status As String) As String
        Return _service.ReadActivityLog(RefineryID, CallerIP, Scenario, UserID, ComputerName, Service, Method, EntityName, PeriodStart, PeriodEnd, Notes, Status)
    End Function

    <WebMethod()> _
    Public Function DataDump(ByVal ReportCode As String, ByVal ds As String, ByVal scenario As String, _
                             ByVal currency As String, ByVal startDate As Date, ByVal UOM As String, _
                             ByVal studyYear As Integer, ByVal includeTarget As Boolean, ByVal includeYTD As Boolean, _
                             ByVal includeAVG As Boolean, ByVal clientKey As String) As DataSet
        Return _service.DataDump(ReportCode, ds, scenario, currency, startDate, UOM, studyYear, includeTarget, includeYTD, _
                                 includeAVG, clientKey)
    End Function


    <WebMethod()> _
    Public Function AuthorizeAndDataDumpLite(ByVal sProc As String, ByVal ds As String, ByVal scenario As String, _
                                             ByVal currency As String, ByVal startYear As Integer, _
                                             ByVal startMonth As Integer, ByVal studyYear As String, _
                                             ByVal UOM As String, ByVal includeTarget As Boolean, _
                                             ByVal includeYTD As Boolean, ByVal includeAVG As Boolean, _
                                            ByVal clientKey As String, ByVal encodedSignedCms As Byte()) As DataSet

        Return _service.AuthorizeAndDataDumpLite(sProc, ds, scenario, currency, startYear, startMonth, studyYear, UOM, includeTarget, _
                                                 includeYTD, includeAVG, clientKey, encodedSignedCms)

    End Function
    <WebMethod()> _
    Public Function DataDumpLite(ByVal sProc As String, ByVal ds As String, ByVal scenario As String, _
                                 ByVal currency As String, ByVal startYear As Integer, ByVal startMonth As Integer, _
                                 ByVal studyYear As String, ByVal UOM As String, ByVal includeTarget As Boolean, _
                                 ByVal includeYTD As Boolean, ByVal includeAVG As Boolean, ByVal clientKey As String) As DataSet

        Return _service.DataDumpLite(sProc, ds, scenario, currency, startYear, startMonth, studyYear, UOM, _
                                     includeTarget, includeYTD, includeAVG, clientKey)
    End Function
    Public Function DataDumpLiteFromDb(ByVal sProc As String, ByVal ds As String, ByVal scenario As String, _
                                       ByVal currency As String, ByVal startYear As Integer, ByVal startMonth As Integer, _
                                       ByVal studyYear As String, ByVal UOM As String, ByVal includeTarget As Boolean, _
                                       ByVal includeYTD As Boolean, ByVal includeAVG As Boolean, ByVal clientKey As String) As DataSet

        Return _service.DataDumpLiteFromDb(sProc, ds, scenario, currency, startYear, startMonth, studyYear, UOM, _
                                           includeTarget, includeYTD, includeAVG, clientKey)
    End Function



    <WebMethod()> _
    Public Function AuthenticateByCertificateAndClientId(ByRef errorResponse As String, ByVal encodedSignedCms As Byte(), _
            ByVal clientKey As String, ByVal displaySysException As Boolean) As Boolean
        Return _service.AuthenticateByCertificateAndClientId(errorResponse, encodedSignedCms, clientKey, displaySysException)

    End Function

    'Public Function GetClientKeyInfoFromToken(clientTokenKey As String) As ClientKey

    '    Dim crypt As New ClientKey
    '    Dim clientKey As New ClientKey

    '    clientKey = crypt.GetClientKeyInfoFromToken(clientTokenKey)

    '    Return clientKey


    'End Function

    'Public Function GetStringContentFromToken(clientTokenKey As String) As String

    '    Dim companyInfo As String

    '    companyInfo = Cryptographer.Decrypt(clientTokenKey)

    '    Return companyInfo

    'End Function

    'Public Function EncryptClientKeyValue(encryptText As String) As String

    '    Dim retVal As String

    '    Dim encrypt As New ProfileLiteSecurity.Cryptography.Cryptographer()

    '    retVal = encrypt.Encrypt(encryptText)

    '    Return retVal

    'End Function

#Region "Intialization"

    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(ProfileLiteWebServices))
       
   
    End Sub
#End Region

#Region "Helper Functions"
    'Sub changeOpexall2()

    '    Me.SqlUpdateCommand24.CommandText = "UPDATE dbo.OpExAll SET CatalystsFCC = @CatalystsFCC, CatalystsHYC = @CatalystsHYC" & _
    '    ", CatalystsNKSHYT = @CatalystsNKSHYT, CatalystsDHYT = @CatalystsDHYT, CatalystsV" & _
    '    "HYT = @CatalystsVHYT, CatalystsRHYT = @CatalystsRHYT, CatalystsHYFT = @Catalysts" & _
    '    "HYFT, CatalystsCDWax = @CatalystsCDWax, CatalystsREF = @CatalystsREF, CatalystsH" & _
    '    "YG = @CatalystsHYG, CatalystsS2Plant = @CatalystsS2Plant, CatalystsPetChem = @Ca" & _
    '    "talystsPetChem, Catalysts = @Catalysts, CatalystsOth = @CatalystsOth, PurOthN2 =" & _
    '    " @PurOthN2, PurOthH2O = @PurOthH2O, PurOthOth = @PurOthOth, PurOth = @PurOth, Ro" & _
    '    "yalties = @Royalties, EmissionsTaxes = @EmissionsTaxes, EmissionsPurch = @Emissi" & _
    '    "onsPurch, EmissionsCredits = @EmissionsCredits, OthVolOth = @OthVolOth, OthVol =" & _
    '    " @OthVol, GANonPers = @GANonPers, InvenCarry = @InvenCarry, Depreciation = @Depr" & _
    '    "eciation, Interest = @Interest, STNonCash = @STNonCash, TotRefExp = @TotRefExp, " & _
    '    "Cogen = @Cogen, OthRevenue = @OthRevenue, ThirdPartyTerminalRM = @ThirdPartyTerm" & _
    '    "inalRM, ThirdPartyTerminalProd = @ThirdPartyTerminalProd, POXO2 = @POXO2, PMAA =" & _
    '    " @PMAA, OthVolDemCrude = @OthVolDemCrude, OthVolDemLightering = @OthVolDemLighte" & _
    '    "ring, OthVolDemProd = @OthVolDemProd, STVol = @STVol, TotCashOpEx = @TotCashOpEx" & _
    '    ", ExclFireSafety = @ExclFireSafety, ExclEnvirFines = @ExclEnvirFines, ExclOth = " & _
    '    "@ExclOth, TotExpExcl = @TotExpExcl, STSal = @STSal, STBen = @STBen, PersCostExcl" & _
    '    "TA = @PersCostExclTA, PersCost = @PersCost, EnergyCost = @EnergyCost, NEOpex = @" & _
    '    "NEOpex WHERE  (SubmissionID = @SubmissionID);"



    '    Me.SqlUpdateCommand24.CommandText += "SELECT CatalystsFCC, CatalystsHYC, CatalystsNKSHYT, Catal" & _
    '    "ystsDHYT, CatalystsVHYT, CatalystsRHYT, CatalystsHYFT, CatalystsCDWax, Catalysts" & _
    '    "REF, CatalystsHYG, CatalystsS2Plant, CatalystsPetChem, Catalysts, CatalystsOth, " & _
    '    "PurOthN2, PurOthH2O, PurOthOth, PurOth, Royalties, EmissionsTaxes, EmissionsPurc" & _
    '    "h, EmissionsCredits, OthVolOth, OthVol, GANonPers, InvenCarry, Depreciation, Int" & _
    '    "erest, STNonCash, TotRefExp, Cogen, OthRevenue, ThirdPartyTerminalRM, ThirdParty" & _
    '    "TerminalProd, POXO2, PMAA, OthVolDemCrude, OthVolDemLightering, OthVolDemProd, S" & _
    '    "TVol, TotCashOpEx, ExclFireSafety, ExclEnvirFines, ExclOth, TotExpExcl, STSal, S" & _
    '    "TBen, PersCostExclTA, PersCost, EnergyCost, NEOpex, Currency, DataType, Scenario" & _
    '    ", SubmissionID FROM dbo.OpExAll WHERE (SubmissionID = @SubmissionID);"
    'End Sub


    'Private Sub LoadData(ByVal sda As SqlDataAdapter, ByVal ds As DataSet, ByVal tablename As String, ByVal submissionId As String, ByVal refineryId As String)
    '    Dim t As Integer
    '    Dim rows() As DataRow


    '    sda.Fill(ds)

    '    If ds.Tables(tablename).Columns.Contains("RefineryID") Then
    '        rows = ds.Tables(tablename).Select("RefineryID='" + refineryId.ToString + "'")
    '    ElseIf ds.Tables(tablename).Columns.Contains("SubmissionID") Then
    '        rows = ds.Tables(tablename).Select("SubmissionID='" + submissionId.ToString + "'")
    '    End If


    '    For t = 0 To rows.Length - 1
    '        rows(t).Delete()
    '    Next
    'End Sub
    'Generically assigns values from one row to another row in a different table or dataset
    'by ColumnName
    'Sub AssignData(ByVal newRow As DataRow, ByVal dataRow As DataRow)

    '    Dim dcol As DataColumn
    '    'TODO: check if column types match before merging

    '    For Each dcol In dataRow.Table.Columns
    '        If newRow.Table.Columns.Contains(dcol.ColumnName) And Not dcol.ColumnName.StartsWith("Submission") Then
    '            'classErr = "Table: " + newRow.Table.TableName + " Column: " + dcol.ColumnName + " ColumnType: " + dcol.DataType.ToString()
    '            newRow(dcol.ColumnName) = DeNull(dataRow, dcol.ColumnName)
    '        End If
    '    Next
    '    'classErr = ""
    'End Sub
#End Region
End Class